console.log("Hello Josh");

console.log(document);
// result: document HTML code

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");

console.log(txtFirstName);
console.log(txtLastName);
// result: input field / tag

/*
	alternative ways:
		>> document.getElementById("txt-first-name");
		>> document.getElementByClassName("text-class");
		>> document.getElementByTagName("h1");
*/

// target the full name 
let spanFullname= document.querySelector("#span-full-name");
console.log(spanFullname);


txtFirstName.addEventListener('keyup', (event) => {
	console.log(event);
	console.log(event.target);
	console.log(event.target.value);
});

// stretch
const keyCodeEvent = (e) => {

	let kc = e.keyCode;
	if (kc === 65) {
		e.target.value = null;
		alert('Someone clicked a');
	}
}

txtFirstName.addEventListener('keyup', keyCodeEvent);


// Activity


const fullName = (e) => {

spanFullname.innerHTML = txtFirstName.value + " " + txtLastName.value;
}


txtFirstName.addEventListener('keyup', fullName);
txtLastName.addEventListener('keyup', fullName);
/*ACTIVITY END*/
